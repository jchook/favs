# Vision

A truly sophisticated favs system would give total control over the download and interpretation process, providing sensible and meaningful defaults for many different sites.

When I say total control though, I make the caveat that it would behove implementors to quickly and efficiently agree upon a flexible standard that gets universally implemented.

This standard should include control over how the data gets:

- fetched
- parsed
- stored
- searched
- viewed
- shared

# Message Queue

Eventually I would like to move to a message queue for processing and webhooks.

We would need to add [prometheus](https://prometheus.io/) and [grafana](https://grafana.com/) or similar software to monitor the cluster.

Requirements:

- Persistence
- Good client libraries for PHP + Node
- At least once delivery

Nice-to-have, but not essential:

- Fault tolerance
- High availability
- High performance
- Not written in Java, Python, or Ruby
- Exactly once delivery

Top choices:

- [NATS Streaming Server](https://nats-io.github.io/docs/nats_streaming/intro.html)

  - Only 3MB docker image
  - Low CPU usage
  - Absurdly high performance (8M messages per second)
  - Written in Go (originally in Ruby)
  - Persistence with Streaming Server add-on
  - High availability
  - At-least-once delivery
  - Historical replay
  - Durable subscriptions that survive server restarts

- RabbitMQ
  - Written in erlang (the langauge of the Internet)
  - Popular and well-known
  - Decent Web GUI built-in
  - Highly configurable message routing
