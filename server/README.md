# 🖥️ Favs Server

Keep a private, searchable archive of everything you like on the Internet.

## Status

| Component         | Status |
| ----------------- | ------ |
| REST API          | 🐣     |
| Media archiver    | 🐣     |
| Website archiver  | 🐣     |
| Webhooks          | 🥚     |
| Search engine     | 💭     |

Range: 💭, 🥚, 🐣, 🐥, 🐔

## Run (Docker)

Change into the `server` directory and run `docker-compose up`. This will start
4 containers:

- http, port 8080
- php-fpm, port 9000
- mysql, port 3306
- archive worker

## CLI

Here is a basic example of how to use the CLI tool (find it in the `cli`
directory):

```sh
# First create a new user
# This will also save your access token to ~/.config/favs/default
favs signup

# Add a new fav to be archived
favs new

# List the most recent favs
favs index
```

## Browser Extension

Load the browser extension into Chrome or Firefox (e.g.
["load unpacked"](https://stackoverflow.com/questions/24577024/install-chrome-extension-form-outside-the-chrome-web-store)).

Click on the icon to enter your server address and login credentials.

Whenever you want to bookmark a fav, simply click the icon while browsing the
page you want to archive.
