ALTER TABLE `favs`
MODIFY `status` enum('new', 'selected', 'error', 'archived') NOT NULL DEFAULT 'new';

ALTER TABLE `webhooks`
MODIFY `status` enum('enabled', 'disabled') NOT NULL DEFAULT 'enabled';
