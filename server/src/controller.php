<?php

// URI
$uri = explode('?', $_SERVER['REQUEST_URI'])[0];

// Request
$req = strtoupper($_SERVER['REQUEST_METHOD']) . ' ' . $uri;


try {

  // Support PATCH and JSON input
  play('input');

  // Authenticate
  play('auth/pre');

  switch ($req) {
    case 'GET /':
    case 'GET /health': return play('health');

    // Auth
    case 'GET /auth/me': return play('auth/me');
    case 'POST /auth/login': return play('auth/login');

    // Favs
    case 'GET /favs': return play('favs/index');
    case 'POST /favs': return play('favs/create');
    case 'PATCH /favs': return play('favs/patch');

    // Users
    case 'POST /users': return play('users/create');

    // Webhooks
    case 'POST /webhooks': return play('webhooks/create');
    case 'GET /webhooks': return play('webhooks/index');

    // 404
    default: throw new NotFound('Route not defined: ' . $req);
  }

} catch (FavsException $e) {
  error_log("Controller caught: $e");
  return [$e->getCode() ?: INTERNAL_SERVER_ERROR, [], json_encode([
    'status' => $e->getCode(),
    'error' => $e->getMessage(),
  ])];
}
