<?php

// Env (optional)
@include __DIR__ . '/.env.php';

// Constants
require_once __DIR__ . '/include/constants/events.php';
require_once __DIR__ . '/include/constants/http.php';
require_once __DIR__ . '/include/constants/paths.php';

// Classes
require_once __DIR__ . '/include/classes/exceptions.php';

// Functions (pure)
require_once __DIR__ . '/include/functions/format.php';
require_once __DIR__ . '/include/functions/validations.php';

// Effects (side-effects)
require_once __DIR__ . '/include/effects/crypto.php';
require_once __DIR__ . '/include/effects/db.php';
require_once __DIR__ . '/include/effects/http.php';
require_once __DIR__ . '/include/effects/play.php';
require_once __DIR__ . '/include/effects/user.php';
require_once __DIR__ . '/include/effects/webhook.php';
