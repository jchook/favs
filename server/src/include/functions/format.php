<?php

/**
 * PHP escapes forward slashes by default for a dumb reason-- turn that off.
 * The reason? So that you can embed JSON into HTML <script> tags. Everyone
 * should properly escape their JSON as shown in Rule 3.1 of OWASP cheatsheet,
 * and relying on this ugly "on-by-default" hack can lead to XSS vulnerability.
 *
 * @link https://www.php.net/manual/en/function.json-encode.php
 * @link https://cheatsheetseries.owasp.org/cheatsheets/Cross_Site_Scripting_Prevention_Cheat_Sheet.html#rule-31---html-escape-json-values-in-an-html-context-and-read-the-data-with-jsonparse
 */
function jsonEncode($input)
{
  return json_encode($input, JSON_UNESCAPED_SLASHES);
}

/**
 * Response based on Sinatra
 * @link http://sinatrarb.com/intro.html#ReturnValues
 */
function formatResponse($arg) {
  $headers = ['Content-Type: application/json'];
  if (is_int($arg)) {
    if ($arg === NO_CONTENT) return [$arg, $headers, ''];
    else return [$arg, $headers, null];
  }
  if (is_array($arg)) {
    switch (count($arg)) {
      case 1: return [$arg[0], $headers, 'null'];
      case 2: return [$arg[0], $headers, jsonEncode($arg[1])];
      case 3: return [$arg[0], array_merge($headers, $arg[1]), jsonEncode($arg[2])];
    }
  }
  return [500, $headers, '{"error": "Internal server error"}'];
}
