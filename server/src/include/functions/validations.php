<?php

/**
 * Given a user-supplied URL, check it and make sure it's normalized
 */
function validUrl(string $url): string
{
  if (!$url) throw new Invalid('URL required');
  $parts = parse_url($url);
  switch ($parts['scheme'] ?? '') {
    case 'https':
    case 'http':
    case 'ftp':
      break;
    default:
      throw new Invalid('Unsupported URL scheme');
  }
  if (empty($parts['host'])) {
    throw new Invalid('Invalid URL format');
  }
  return $url;
}

/**
 * Validate a webhook event name
 */
function validEventName(string $eventName): string
{
  switch ($eventName) {
    case WEBHOOK_EVENT_FAV:
      return $eventName;
    default:
      throw new Invalid('Invalid event name');
  }
}
