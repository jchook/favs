<?php

class FavsException extends Exception {
  protected $status = INTERNAL_SERVER_ERROR;
  public function __construct($msg = '', $code = null, $prev = null) {
    parent::__construct($msg, is_null($code) ? $this->status : $code, $prev);
  }
}

class QueryException extends FavsException {}
class Unauthorized extends FavsException { protected $status = UNAUTHORIZED; }
class BadRequest extends FavsException { protected $status = BAD_REQUEST; }
class NotFound extends FavsException { protected $status = NOT_FOUND; }
class Invalid extends FavsException { protected $status = INVALID; }
class Forbidden extends FavsException { protected $status = FORBIDDEN; }
class Conflict extends FavsException { protected $status = CONFLICT; }
