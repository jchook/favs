<?php

function generateNonce($length = 32)
{
  $result = null;
  $replace = array('/', '+', '=');
  while(!isset($result[$length-1])) {
  $result .= str_replace($replace, NULL, base64_encode(random_bytes($length)));
  }
  return substr($result, 0, $length);
}
