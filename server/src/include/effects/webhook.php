<?php

/**
 * Do dat dere webhuuk
 * @throws QueryException
 */
function webhook(mysqli $db, int $userId, string $eventName, array $data = []): void
{
  $webhooks = fetchAll($db,
    "SELECT url FROM webhooks WHERE userId = ? AND eventName = ? AND status = 'enabled' LIMIT ?",
    'isi', $userId, $eventName, $_SERVER['FAVS_WEBHOOK_LIMIT'] ?? 100
  );

  // Foreach webhook
  foreach ($webhooks as $webhook) {

    // cURL the target url
    $ch = curl_init();
    curl_setopt_array($ch, [
      CURLOPT_URL => $webhook['url'],
      CURLOPT_POST => true,
      CURLOPT_SAFE_UPLOAD => true, // prevents use of @
      CURLOPT_POSTFIELDS => $data, // accepts array
    ]);
    curl_exec($ch);
    $errorCode = curl_errno($ch);
    $responseCode = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
    curl_close($ch);

    // Record the outcome of it
    query($db,
      "INSERT INTO webhookCalls (webhookId, errorCode, responseCode) VALUES (?, ?, ?)",
      'iii', $webhook['id'], $errorCode ?: 0, $responseCode ?: 0
    );
  }
}
