<?php

/**
 * HTTP response
 */
function respond(int $status, array $headers, $body)
{
  http_response_code($status);
  array_walk($headers, 'header');
  echo (string)$body;
}
