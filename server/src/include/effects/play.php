<?php

require_once __DIR__ . '/db.php';

/**
 * Run a play
 */
function play($playName, array $context = [])
{
  extract($context, EXTR_SKIP);
  return formatResponse(include SRCPATH . '/plays/' . $playName . '.php');
}
