<?php

/**
 * Current user
 */
function user(): array
{
  if (empty($_SERVER['USER_ID'])) {
    throw new Unauthorized('Please authenticate first.');
  }
  if (!empty($GLOBALS['user'])) return $GLOBALS['user'];
  try {
    return $GLOBALS['user'] = fetch(db(),
      'SELECT id, username, createdAt FROM users WHERE id = ?',
      'i', $_SERVER['USER_ID']
    );
  } catch (NotFound $e) {
    throw new Unauthorized('Please authenticate first..');
  }
}
