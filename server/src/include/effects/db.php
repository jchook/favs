<?php

/**
 * Get the global database connection
 */
function db()
{
  // Already instantiated?
  if (isset($GLOBALS['db'])) {
    return $GLOBALS['db'];
  }

  // DB access
  $db = new mysqli(
    'p:'.($_SERVER['MYSQL_HOST'] ?? '127.0.0.1'),
    $_SERVER['MYSQL_USERNAME'] ?? 'favs',
    $_SERVER['MYSQL_PASSWORD'] ?? 'favs',
    $_SERVER['MYSQL_DATABASE'] ?? 'favs',
  );

  // Check for errors
  if ($db->connect_errno) {
    echo "Failed to connect to MySQL: (" . $db->connect_errno . ") " .
      $db->connect_error . "\n";
    exit(1);
  }

  // Expose $db to the entire application
  return $GLOBALS['db'] = $db;
}

/**
 * Query MySQL using prepared statements.
 *
 * Returns false on error.
 * Returns mysqli_result on success
 *
 * @return mysqli_result|bool
 * @throws QueryException
 */
function query(mysqli $db, string $sql, string $types = '', ...$params)
{
  // Try to prepare the statement
  $stmt = $db->prepare($sql);
  if (!$stmt) {
    throw new QueryException('Cannot prepare statement: ' . $db->error);
  }

  // Try to bind params
  if ($types) {
    $result = $stmt->bind_param($types, ...$params);
    if (!$result) {
      throw new QueryException('Cannot bind param: ' . $db->error);
    }
  }

  // Try to execute the query
  $result = $stmt->execute()
    ? ($stmt->get_result() ?: $stmt->result_metadata() ?: true)
    : false
  ;

  // Trigger error
  if ($result === false) {
    throw new QueryException('Query error: ' . $db->error);
  }

  // Always close the statement
  $stmt->close();

  // Return mysqli_result, true, or false
  return $result;
}

/**
 * Fetch all records
 * @throws QueryException
 */
function fetchAll(mysqli $db, string $sql, string $types = '', ...$params): array
{
  $result = query($db, $sql, $types, ...$params);
  $records = $result->fetch_all(MYSQLI_ASSOC);
  $result->free();
  return $records ?: [];
}

/**
 * Fetch only the first record
 * @throws NotFound
 * @throws QueryException
 */
function fetch(mysqli $db, string $sql, string $types = '', ...$params): array
{
  $result = query($db, $sql, $types, ...$params);
  $record = $result->fetch_assoc();
  $result->free();
  if (!$record) {
    throw new NotFound('Record not found');
  }
  return $record;
}

function getInsertId(mysqli $db)
{
  return $db->insert_id;
}
