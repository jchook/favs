<?php

try {

  // Bootstrap
  require_once __DIR__ . '/bootstrap.php';

  // Execute and respond
  respond(...require __DIR__ . '/controller.php');

} catch (Throwable $e) {
  error_log("Global handler caught: $e");
  http_response_code(500);
  header('Content-Type: application/json');
  echo '{"error": "Internal server error"}';
}
