<?php

/**
 * Health check
 */

// Make sure we have communication with the DB
query(db(), 'SELECT 1 + 1');

// If an error occurred, we wont reach this statement
return [OK, ['ok' => true]];
