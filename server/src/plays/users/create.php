<?php

/**
 * Sign-up
 */

// IDEA: We should have a way to create invite codes

// Server owners can completely disable sign-up
if (!empty($_SERVER['SIGNUP_DISABLED'])) {
  throw new Forbidden('Sign-up disabled');
}

extract($_POST);

if (empty($username)) {
  throw new Invalid('username required');
}

if (empty($password)) {
  throw new Invalid('password required');
}

if (strlen($username) > 64) {
  throw new Invalid('username too long');
}

$passwordHash = password_hash($password, PASSWORD_DEFAULT);

try {
  query(db(),
    'INSERT INTO users (username, passwordHash) VALUES (?, ?)', 'ss',
    $username, $passwordHash
  );
} catch (Exception $e) {
  throw new Conflict('Username already taken');
}

return [CREATED, [
  'id' => getInsertId(db()),
  'username' => $username,
]];
