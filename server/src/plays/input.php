<?php

// STDIN

// Enable input
if (!isset($_SERVER['REQUEST_BODY'])) {
  $read = [fopen('php://input', 'r')];

  // CLI SAPI?
  // https://www.php.net/manual/en/features.commandline.io-streams.php
  if (defined('STDIN')) {
    $read = [STDIN];
    $write = $except = null;
    if (stream_select($read, $write, $except, 0) > 0) {
      $_SERVER['REQUEST_BODY'] = stream_get_contents(current($read));
    }
    unset($read, $write, $except);
  } else {
    $fp = fopen('php://input', 'r');
    $_SERVER['REQUEST_BODY'] = stream_get_contents($fp);
    fclose($fp);
    unset($fp);
  }
}

// Parse REQUEST_BODY
if (!empty($_SERVER['REQUEST_BODY'])) {
  if (strpos($_SERVER['HTTP_CONTENT_TYPE'] ?? '', 'application/json') !== false) {
    $_POST = json_decode($_SERVER['REQUEST_BODY'], true);
    if (json_last_error() !== JSON_ERROR_NONE) {
      throw new BadRequest('JSON parse error: ' . json_last_error_msg());
    }
  } else {
    parse_str($_SERVER['REQUEST_BODY'], $_POST);
  }
}
