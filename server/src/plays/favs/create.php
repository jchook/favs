<?php

$user = user();

// Validate the URL
$url = validUrl($_POST['url'] ?? '');

// Notes
$notes = $_POST['notes'] ?? '';

// Insert the row
query(db(),
  'INSERT INTO favs (userId, url, notes) VALUES (?, ?, ?)', 'iss',
  $user['id'], $url, $notes
);

// Return the ID
return [CREATED, [
  'id' => getInsertId(db()),
  'url' => $url,
  'notes' => $notes,
]];
