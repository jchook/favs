<?php

$user = user();

$sql = '
  SELECT id, status, url, notes, createdAt
  FROM favs
  WHERE userId = ?
  AND id <= ?
  AND id >= ?
  ORDER BY id ASC
  LIMIT 100
';

$favs = fetchAll(
  db(), $sql, 'iii',
  $user['id'],
  $_GET['maxId'] ?? PHP_INT_MAX,
  $_GET['minId'] ?? 0
);

return [OK, $favs];
