<?php

$user = user();

if (empty($_POST['id'])) {
  throw new Invalid('Missing id');
}

// Update notes
if (isset($_POST['notes'])) {
  query(db(),
    'UPDATE favs SET notes = ? WHERE id = ? AND userId = ?', 'sii',
    $_POST['notes'], $_POST['id'], $user['id']
  );
}

return OK;
