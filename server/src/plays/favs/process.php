<?php

/**
 *
 * Process all the shiz that we bookmark.
 *
 * Status:
 *  0 = unclaimed
 *  1 = claimed
 *  2 = processing
 *  3 = ready
 *  4 = error
 *
 */

// Get the DB
$db = db();

// Debug goes to STDERR
function debug($info) {
  fwrite(STDERR, $info . "\n");
}


// ---


// Which processor # are we?
$procId = (int)($_SERVER['FAVS_PROCESSOR_ID'] ?? mt_rand(0, 65535));

// How many bookmarks to process at once?
$procLimit = (int)($_SERVER['FAVS_PROCESSOR_LIMIT'] ?? 1);

// How to store the archive data -- command that takes NO arguments
// All necessary data MUST pass through environment variables, e.g. id and url
$archiveCmd = $_SERVER['FAVS_ARCHIVE_CMD'] ?? BINPATH . '/archive-url';

// Where to store the archive
$archivePath = $_SERVER['FAVS_ARCHIVE_PATH'] ?? APPPATH . '/archive';


// ---


// Did we already claim records and we're just being a failing idiot?
$favs = fetchAll($db,
  "SELECT id, userId, url, notes FROM favs WHERE status = 'selected' AND processor = ? LIMIT ?",
  'ii', $procId, $procLimit
);

// No? Okay claim some rows
if (!$favs) {

  // Atomic operation to claim records
  query($db,
    "UPDATE favs SET processor = ?, status = 'selected' WHERE status = 'new' ORDER BY id ASC LIMIT ?",
    'ii', $procId, $procLimit
  );

  // Select the records we just claimed
  $favs = fetchAll($db,
    "SELECT id, userId, url, notes FROM favs WHERE status = 'selected' AND processor = ? LIMIT ?",
    'ii', $procId, $procLimit
  );
}

$favsCount = count($favs);
$passCount = 0;

if (!$favsCount) {
  debug("No favs to process");
  return;
}

debug("Processing $favsCount favs...");

// For each claimed record
foreach ($favs as $fav) {

  // Which URL to download?
  $url = $fav['url'];
  echo "[$procId] processing $url\n";

  // User's archive path
  $userArchive = $archivePath . '/' . $fav['userId'];
  if (!is_dir($userArchive)) {
    if (!mkdir($userArchive, 0700, true)) {
      throw new Exception('Unable to find or create the archive directory.');
    }
  }

  // Download the website
  $cmd = implode(' ', [
    'id=' . escapeshellarg($fav['id']),
    'url=' . escapeshellarg($fav['url']),
    'notes=' . escapeshellarg($fav['notes']),
    'ARCHIVE_PATH=' . escapeshellarg($archivePath . '/' . $fav['userId']),
  ]) . ' ' . escapeshellarg($archiveCmd);
  fwrite(STDERR, $cmd . "\n");
  system($cmd, $code);
  if ($code) {
    trigger_error("Trouble downloading ($code) $url", E_USER_NOTICE);
    query($db,
      "UPDATE favs SET status = 'error' WHERE id = ?", 'i' , $fav['id']
    );
  } else {
    $passCount++;
    query($db,
      "UPDATE favs SET status = 'archived' WHERE id = ?", 'i' , $fav['id']
    );
  }

  try {
    webhook($db, $fav['userId'], 'fav');
  } catch (Throwable $t) {
    trigger_error("Webhook failed: $t", E_USER_NOTICE);
  }
}

debug("{$passCount}/{$favsCount} successfully processed");
