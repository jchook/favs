<?php

/**
 * Run this code before other plays to authenticate the current user
 */

// Already logged-in?
if (!empty($_SERVER['USER_ID'])) return;

// No auth token?
if (empty($_SERVER['HTTP_X_AUTH_TOKEN'])) return;
if ($_SERVER['HTTP_X_AUTH_TOKEN'] === '-') return;

try {

  // Login
  $_SERVER['USER_ID'] = fetch(db(),
    'SELECT userId FROM authTokens WHERE token = ? AND expiresAt > NOW()', 's',
    $_SERVER['HTTP_X_AUTH_TOKEN']
  )['userId'];

} catch (NotFound $e) {
  // Expired or invalid auth token
}
