<?php

/**
 * Log-in as a user.
 *
 * This will create a new authToken and return it.
 */

// Get the user
$user = fetch(db(),
  'SELECT id, passwordHash FROM users WHERE username = ? LIMIT 1', 's',
  $_POST['username']
);

// Password matches?
if (!password_verify($_POST['password'], $user['passwordHash'])) {
  throw new Unauthorized('Invalid password');
}

// Create a new token
$token = generateNonce();
query(db(), 'INSERT INTO authTokens (userId, token, expiresAt) VALUES (?, ?, ?)', 'iss',
  $user['id'], $token, date('Y-m-d H:i:s', time() + (60 * 60 * 24 * 365))
);

// Return the token
return [OK, compact('token')];
