<?php

/**
 * Get the currently authenticated user
 */

$user = user();

return [OK, [
  'id' => $user['id'],
  'username' => $user['username'],
  'createdAt' => $user['createdAt'],
]];
