<?php

$user = user();

$sql = '
  SELECT id, status, eventName, url, createdAt
  FROM webhooks
  WHERE userId = ?
  AND id <= ?
  AND id >= ?
  ORDER BY id ASC
  LIMIT 100
';

$webhooks = fetchAll(
  db(), $sql, 'iii',
  $user['id'],
  $_GET['maxId'] ?? PHP_INT_MAX,
  $_GET['minId'] ?? 0
);

return [OK, $webhooks];
