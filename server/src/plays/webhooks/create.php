<?php

$user = user();
$db = db();

if (empty($_POST['eventName'])) {
  throw new Invalid('Invalid eventName');
}

$url = validUrl($_POST['url'] ?? '');
$eventName = validEventName($_POST['eventName'] ?? '');

// Insert the row
query($db,
  'INSERT INTO webhooks (userId, eventName, url) VALUES (?, ?, ?)', 'iss',
  $user['id'], $eventName, $url
);

// Return the ID
return [CREATED, [
  'id' => getInsertId($db),
  'status' => 'enabled',
  'eventName' => $eventName,
  'url' => $url,
]];
