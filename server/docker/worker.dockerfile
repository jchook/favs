FROM php:7.4-cli-alpine

# Installs latest Chromium package.
RUN echo "http://dl-cdn.alpinelinux.org/alpine/edge/main" > /etc/apk/repositories \
    && echo "http://dl-cdn.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories \
    && echo "http://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories \
    && echo "http://dl-cdn.alpinelinux.org/alpine/v3.12/main" >> /etc/apk/repositories \
    && apk upgrade -U -a \
    && apk add \
    libstdc++ \
    chromium \
    harfbuzz \
    nss \
    freetype \
    ttf-freefont \
    font-noto-emoji \
    wqy-zenhei

# Youtube-dl, bash, wget
RUN apk add bash wget python3 py3-pip \
		&& pip3 install youtube-dl 

# Install php extensions
RUN apk add gcc g++ autoconf make libmemcached-dev zlib-dev \
	  && docker-php-ext-install -j$(nproc) mysqli \
		&& pecl install memcached \
		&& docker-php-ext-enable memcached 

# Clear out any files we can
RUN rm -rf /var/cache/* \
    && mkdir /var/cache/apk 

# Copy project to /opt/favs 
COPY . /opt/favs

# Add favs as a user
RUN adduser -D favs \
    && chown -R favs:favs /opt/favs \
		&& echo "" > /var/spool/cron/crontabs/root \
		&& echo "* * * * * /opt/favs/bin/process-favs" > /var/spool/cron/crontabs/favs

VOLUME /archive
ENTRYPOINT /opt/favs/docker/worker-entrypoint.sh

# Run crond
# CMD [ "crond", "-f" ]

