FROM php:7.4-fpm-alpine
RUN apk update \
      && apk add gcc g++ autoconf make libmemcached-dev zlib-dev \
	    && docker-php-ext-install -j$(nproc) mysqli \
	  	&& pecl install memcached \
	  	&& docker-php-ext-enable memcached 

COPY . /opt/favs
