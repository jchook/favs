# Docker

I have made images for:

- `http` web server
- `php` fpm server
- `worker` background processing
- `mysql` database

## Info

So far I chose the php:7.4-fpm-alpine images as a base. You can read more about
how that works here:

- https://hub.docker.com/_/php
- https://github.com/docker-library/docs/blob/master/php/README.md


Makefile is neat:
 
- https://learnxinyminutes.com/docs/make/
- https://itnext.io/docker-makefile-x-ops-sharing-infra-as-code-parts-ea6fa0d22946

Docker is neat:

- https://docs.docker.com/compose/compose-file/
- https://docs.docker.com/engine/reference/builder/
- https://hub.docker.com/_/mysql


## Run

First copy `.env.sample` to `.env` and update it as needed.

```sh
# To build and run all the containers (in `server` directory):
docker-compose up

# To run an individual service:
docker-compose up http

# For individual containers you gotta go nuts with docker flags:
docker run \
  -it --env-file .env \
  -v "$HOME"/projects/favs/server/etc/nginx/templates:/etc/nginx/templates \
  server_http:latest bash
```

## Build

You can build any of these images from the `server` directory, e.g.

```sh
docker build --env-file .env -t favs/php:1 -f docker/php.dockerfile .
```

