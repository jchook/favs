FROM mysql:latest
COPY ./etc/mysql/schema/favs.sql /docker-entrypoint-initdb.d/favs.sql
