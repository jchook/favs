# 🌠 Favs

A private, searchable, high-fidelity archive of everything you like on the
Internet.

- Ultra-simple
- Self-hosted, free and open-source
- Works with iOS, Android, Web, Chrome, Linux, Mac, and Windows

## Status

The favs ecosystem contains a handful of major components.

We have taken an archive-first approach, ensuring that we can (securely and
privately) capture favs ASAP and figure out how to search them later.

| Component      | Status |
| -------------- | ------ |
| Server backend | 🐣     |
| Browser plugin | 🐣     |
| CLI client     | 🐣     |
| iOS client     | 🥚     |
| Android client | 💭     |
| Web client     | 💭     |

Range: 💭, 🥚, 🐣, 🐥, 🐔
