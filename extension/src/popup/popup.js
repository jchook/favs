// Chrome and FF have a very similar API
var BROWSER = chrome || browser;

// Elements
var popup = document.getElementById('popup');
var settings = document.getElementById('settings');
var settingsApiUrl = document.getElementById('settingsApiUrl');
var settingsUsername = document.getElementById('settingsUsername');
var settingsPassword = document.getElementById('settingsPassword');
var settingsForm = document.getElementById('settingsForm');
var favButton = document.getElementById('favButton');
var favdForm = document.getElementById('favdForm');
var favdNotes = document.getElementById('favdNotes');

function createLocalStorageGetter(key) {
  return function() {
    return window.localStorage.getItem(key);
  };
}
function createLocalStorageSetter(key) {
  return function(input) {
    return typeof input === 'undefined' || input === null
      ? window.localStorage.removeItem(key)
      : window.localStorage.setItem(key, input);
  };
}

var getUsername = createLocalStorageGetter('myUsername');
var setUsername = createLocalStorageSetter('myUsername');
var getApiUrl = createLocalStorageGetter('myApiUrl');
var setApiUrl = createLocalStorageSetter('myApiUrl');
var getAuthToken = createLocalStorageGetter('myAuthToken');
var setAuthToken = createLocalStorageSetter('myAuthToken');

function getView() {
  return popup.getAttribute('data-view');
}

function setView(name) {
  popup.setAttribute('data-view', name);
}

function apiFetch(url, options) {
  return new Promise(function(resolve, reject) {
    chrome.runtime.sendMessage(
      {
        type: 'apiFetch',
        url: url,
        options: options
      },
      function(response) {
        if (response && response.error) {
          console.error(response.error);
          reject(response.error);
        } else {
          resolve(response);
        }
      }
    );
  });
}

// Load the existing API URL
settingsApiUrl.value = getApiUrl();
settingsUsername.value = getUsername();

// Set the API URL
settingsForm.addEventListener('submit', function(event) {
  event.preventDefault();
  var url = settingsApiUrl.value;
  var username = settingsUsername.value;
  var password = settingsPassword.value;
  setAuthToken();
  doLogin(url, username, password)
    .then(function(data) {
      setApiUrl(url);
      setUsername(username);
      setAuthToken(data.token);
      settings.classList.add('saveSuccess');
      setTimeout(() => settings.classList.remove('saveSuccess'), 1000);
    })
    .catch(function(err) {
      console.error(err);
      settings.classList.add('saveError');
      setTimeout(() => settings.classList.remove('saveError'), 1000);
    });
});

// Login to favs
function doLogin(url, username, password) {
  return apiFetch(url + '/auth/login', {
    method: 'POST',
    body: {
      username: username,
      password: password
    }
  });
}

// Most recent fav id (for patch)
var favdId;

// Do we want to auto-close the popup?
// By focusing on notes textarea, we disable auto-close
var favdAutoClose = true;

/**
 * Fav a thing
 */
function doFav() {
  return new Promise(function(resolve, reject) {
    // Configuration
    var apiUrl = getApiUrl();
    if (!apiUrl) return reject(new Error('missing apiUrl'));
    chrome.tabs.query({ active: true, currentWindow: true }, function(tabs) {
      apiFetch(apiUrl + '/favs', {
        headers: {
          'X-Auth-Token': getAuthToken()
        },
        method: 'POST',
        body: {
          url: tabs[0].url
        }
      })
        .then(function(data) {
          favdId = data.id;
          resolve(data);
        })
        .catch(reject);
    });
  });
}

function doFavPatch() {
  return new Promise(function(resolve, reject) {
    if (!favdId) {
      console.error('Missing favdId');
      return reject(new Error('Missing favdId'));
    }
    apiFetch(getApiUrl() + '/favs', {
      method: 'PATCH',
      body: {
        id: favdId,
        notes: favdNotes.value
      }
    })
      .then(function(data) {
        resolve(data);
        window.close();
      })
      .catch(function(err) {
        console.error(err);
        reject(err);
      });
  });
}

// Default behavior
if (getAuthToken()) {
  setView('loading');
  doFav()
    .then(function() {
      setView('favd');
      setTimeout(function() {
        if (getView() === 'favd' && favdAutoClose) {
          window.close();
        }
      }, 3000);
    })
    .catch(function(err) {
      if (getView() === 'loading') {
        setView('error');
      }
    });
} else {
  setView('settings');
}

// Settings link
document.body.addEventListener('click', function(event) {
  if (event.target.tagName.toUpperCase() === 'A') {
    if (event.target.classList.contains('settingsLink')) {
      event.preventDefault();
      setView('settings');
    }
  }
});

// Notes (pressing enter submits form)
favdNotes.addEventListener('keypress', function(event) {
  if (event.which == 13 && !event.shiftKey) {
    event.preventDefault();
    doFavPatch();
  }
});
favdNotes.addEventListener('focus', function(event) {
  favdAutoClose = false;
});

// Notes (update them)
favdForm.addEventListener('submit', function(event) {
  event.preventDefault();
  doFavPatch();
});
