/* global chrome, browser */

// Chrome and FF have a very similar API
var BROWSER = chrome || browser;

function createLocalStorageGetter(key) {
  return function() {
    return window.localStorage.getItem(key);
  };
}

function createLocalStorageSetter(key) {
  return function(input) {
    return typeof input === 'undefined' || input === null
      ? window.localStorage.removeItem(key)
      : window.localStorage.setItem(key, input);
  };
}

var getAuthToken = createLocalStorageGetter('myAuthToken');
var setAuthToken = createLocalStorageSetter('myAuthToken');
var getApiUrl = createLocalStorageGetter('myApiUrl');
var setApiUrl = createLocalStorageSetter('myApiUrl');

/**
 * HttpError class
 */
function HttpError(message, response, responseBody) {
  this.message = message;
  this.response = response;
  this.responseBody = responseBody;
}

/**
 * Utility function for fetch() chains
 */
function parseJson(response) {
  var parsed = response && response.json && response.json();
  if (response.ok) {
    return parsed;
  }
  var resp = {
    ok: false,
    status: response.status
    // add more response data here as needed...
    // the entire response object cannot pass through chrome's message system
  };
  if (parsed && parsed.error) {
    throw new HttpError(parsed.error, resp, parsed);
  }
  throw new HttpError(
    'Request failed with status: ' + response.status,
    resp,
    parsed
  );
}

/**
 * Send a message to the browser extension's content page
 */
function sendMessage(tabId, message, callback) {
  BROWSER.tabs.sendMessage(tabId, message, callback);
}

/**
 * Store recent favs in an array so that we can easily edit notes
 */
var favsByUrl = {};

/**
 * Do an API fetch. Always resolves.
 */
function apiFetch(url, options) {
  options = options || {};
  if (options.body) {
    if (!options.headers) options.headers = {};
    options.headers['Content-Type'] = 'application/json';
    options.body = JSON.stringify(options.body);
  }
  return fetch(url, options).then(parseJson);
}

async function doLogin(req) {
  var url = req.url;
  var username = req.username;
  var password = req.password;
  setAuthToken();
  const data = await apiFetch(url + '/auth/login', {
    method: 'POST',
    body: {
      username: username,
      password: password
    }
  });
  setApiUrl(url);
  setAuthToken(data.token);
  return data;
}

async function doFav(req) {
  var url = req.url;
  if (favsByUrl[url]) {
    return favsByUrl[url];
  }
  const data = await apiFetch(url + '/favs', {
    method: 'POST',
    body: {
      url: url
    }
  });
  favsByUrl[url] = data;
  return data;
}

async function doFavPatch(req) {
  const favId = req.id;
  const notes = req.notes;
  const data = await apiFetch(getApiUrl() + '/favs', {
    method: 'PATCH',
    body: {
      id: favdId,
      notes: favdNotes.value
    }
  });
}

function handleAction(req) {
  switch (req.type) {
    case 'postFav':
      return doFav(req);
    case 'patchFav':
      return doFavPatch(req);
    case 'login':
      return doLogin(req);
    case 'apiFetch':
      return apiFetch(req.url, req.options);
    default:
      return Promise.resolve();
  }
}

BROWSER.runtime.onMessage.addListener(function(req, sender, sendResponse) {
  console.log('message', req)
  handleAction(req)
    .then(sendResponse)
    .catch(err => sendResponse({ error: err }));
  return true;
});
