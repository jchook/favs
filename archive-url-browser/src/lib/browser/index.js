module.exports = {
  puppeteer: require('./puppeteer'),
  createBrowser: require('./createBrowser'),
  createPage: require('./createPage'),
}
// export { getSolutions as anticaptchaProvider } from './anticaptchaProvider'
