async function createPage(browser, config = {}) {
  const page = await browser.newPage(config)

  await page.setViewport({ width: 1920, height: 1080 })

  page.on('error', err => {
    throw new Error(err)
  })

  page.on('pageerror', err => {
    console.warn('Page error', err)
  })

  return page
}

module.exports = createPage
