const path = require('path')
const puppeteer = require('./puppeteer')

// Support for pkg
// https://github.com/zeit/pkg/issues/204#issuecomment-536323464
const executablePath =
  process.env.PUPPETEER_EXECUTABLE_PATH ||
  (process.pkg
    ? path.join(
        path.dirname(process.execPath),
        'puppeteer',
        ...puppeteer
          .executablePath()
          .split(path.sep)
          .slice(6), // /snapshot/project/node_modules/puppeteer/.local-chromium
      )
    : puppeteer.executablePath())

/**
 * To use a proxy, check out https://www.npmjs.com/package/proxy-from-env
 */
async function createBrowser(config) {
  const args = [
    // This can help allow headless: false
    // https://github.com/berstend/puppeteer-extra/issues/62#issuecomment-534109689
    '--lang=en-US,en;q=0.9',
  ]
  return await puppeteer.launch({
    args,
    executablePath,
    ...config,
  })
}

module.exports = createBrowser
