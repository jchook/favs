const puppeteer = require('puppeteer-extra')
const pluginStealth = require('puppeteer-extra-plugin-stealth')
// const pluginRecaptcha = require('puppeteer-extra-plugin-recaptcha')
// import { getSolutions, PROVIDER_ID } from './recaptchaProvider'

// Try to mask puppeteer from browser detection
// https://github.com/berstend/puppeteer-extra
puppeteer.use(pluginStealth())

// Use a helper to really nail reCAPTCHA 2.0
// https://github.com/berstend/puppeteer-extra/blob/c5828dd01333b6dbb15d74e63d9b866b2b2ff2a6/packages/puppeteer-extra-plugin-recaptcha/src/types.ts#L31
// puppeteer.use(
//   pluginRecaptcha({
//     throwOnError: true,
//     visualFeedback: true,
//     provider: {
//       id: PROVIDER_ID,
//       fn: getSolutions,
//     },
//   }),
// )

// Only use one special instance of puppeteer
module.exports = puppeteer
