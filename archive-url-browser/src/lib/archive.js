/**
 * @link https://www.npmjs.com/package/proxy-from-env
 */
const path = require('path')
const fs = require('fs')
const mkdirp = require('./mkdirp')
const { createBrowser, createPage } = require('./browser')
const createDebug = require('debug')

const debug = createDebug('favs:archive:page')

/**
 * Save as MHTML
 * @link https://chromedevtools.github.io/devtools-protocol/tot/Page#method-captureSnapshot
 */
async function saveMhtml(page, savePath) {
  const cdp = await page.target().createCDPSession()
  const { data } = await cdp.send('Page.captureSnapshot', { format: 'mhtml' })
  fs.writeFileSync(savePath, data)
}

/**
 * Save as JPG
 * @link https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md#pagescreenshotoptions
 */
async function saveJpg(page, savePath) {
  await page.screenshot({
    path: savePath,
    fullPage: true,
  })
}

/**
 * Save as TXT
 * @link https://github.com/GoogleChrome/puppeteer/issues/489
 */
async function saveTxt(page, savePath) {
  const innerText = await page.evaluate('document.body.innerText')
  fs.writeFileSync(savePath, innerText)
}

/**
 * Save a JSON info file
 */
async function saveJsonInfo(info, savePath) {
  fs.writeFileSync(savePath, JSON.stringify(info, null, 2))
}

/**
 * Archive page (entry)
 */
async function archive(opt = {}) {
  const browser = await createBrowser()
  const page = await createPage(browser)
  try {
    const archivePath = opt.ARCHIVE_PATH || 'archive'
    const favId = opt.id || '1'
    const favUrl = opt.url || ''
    const favPath = path.join(archivePath, 'by-id', favId)
    const tags = opt.tags ? opt.tags.split(',') : []

    // Make sure the directory exists
    debug('Creating directory ' + favPath)
    mkdirp(favPath)

    // Navigate to the page
    debug('Navigating to ' + favUrl)
    await page.goto(favUrl, {
      waitUntil: 'networkidle2',
      timeout: 10000,
    })
    debug('Pageload complete')

    // Save JSON info
    debug('[JSON] Saving...')
    const info = {
      id: favId,
      url: favUrl,
      effectiveUrl: page.url(),
      title: await page.title(),
      tags,
    }
    await saveJsonInfo(info, path.join(favPath, favId + '.json'))
    debug('[JSON] Saved')

    // Save a TXT
    debug('[TXT] Saving...')
    await saveTxt(page, path.join(favPath, favId + '.txt'))
    debug('[TXT] Saved')

    // Save a Jpeg
    debug('[JPG] Saving...')
    await saveJpg(page, path.join(favPath, favId + '.jpg'))
    debug('[JPG] Saved')

    // Save MHTML archive
    debug('[MHTML] Saving...')
    await saveMhtml(page, path.join(favPath, favId + '.mhtml'))
    debug('[MHTML] Saved')
  } catch (err) {
    console.error(err)
  } finally {
    await page.close()
    await browser.close()
  }
}

module.exports = archive
