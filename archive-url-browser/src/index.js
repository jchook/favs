try {
  if (process.pkg) {
    require('./require')
  }
  require('./lib/archive')(process.env)
} catch (err) {
  console.error(err)
}
