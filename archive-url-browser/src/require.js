// Puppeteer stealth plugin uses *dynamic* require()
//
// This means it uses logic to determine what to load at runtime, and pkg
// cannot statically analyze its dependencies. As a result, we need to manually
// require all of the potentially require()'d  files here.
//
// ➜ find node_modules/puppeteer-extra-plugin-stealth/evasions -iname '*.js'
//
require('puppeteer-extra-plugin-stealth/evasions/navigator.webdriver')
require('puppeteer-extra-plugin-stealth/evasions/user-agent')
require('puppeteer-extra-plugin-stealth/evasions/chrome.runtime')
require('puppeteer-extra-plugin-stealth/evasions/console.debug')
require('puppeteer-extra-plugin-stealth/evasions/webgl.vendor')
require('puppeteer-extra-plugin-stealth/evasions/window.outerdimensions')
require('puppeteer-extra-plugin-stealth/evasions/navigator.permissions')
require('puppeteer-extra-plugin-stealth/evasions/_template')
require('puppeteer-extra-plugin-stealth/evasions/navigator.plugins')
require('puppeteer-extra-plugin-stealth/evasions/navigator.languages')
